# bus
Dissatisfied with the current non-mobile and slow official CTA bus tracker, I made mine that just tracks the eastbound 60 bus from the 15th St. stop using Firebase, service worker, and solid modern web performance practices.
